"use strict";

const arrTitle = Array.from(document.getElementsByClassName("tabs-title"));

const arrContent = Array.from(document.querySelectorAll(".tabs-content>li"));
arrContent.forEach((el) => (el.style.display = "none"));
arrContent[0].style.display = "block";

arrTitle.forEach((element) => {
  element.addEventListener("click", function () {
    arrTitle.forEach((el) => (el.className = "tabs-title"));
    element.className += " active";

    arrContent.forEach((display) => (display.style.display = "none"));

    document.querySelector(
      `.tabs-content li:nth-child(${arrTitle.indexOf(element) + 1})`
    ).style.display = "block";
  });
});
